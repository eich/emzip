package emzip

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

// A Generator holds all parameters required for code generation.
// Call Generate to run the generator with the given params.
type Generator struct {

	// PackageName to use in generated code.
	// Defaults to the directory name where the generated file is written.
	PackageName string

	// Name of the variable holding the zipped bytes.
	// Defaults to resources
	Name string

	// OutputFilePath is the full path to the file to generate.
	// By default, files are written in the current working dir.
	OutputFilePath string

	// Includes is a list of files and directories to include in the
	// generated zip.
	Includes []string

	// Template text to overwrite the default generator template.
	// Uses the text/template language, available variables are
	// all fields of this Generator struct.
	Template string

	// Value is the string value holding the raw zip data.
	Value string
}

// Generate creates a Go source file with a var storing zip data.
// This makes the embedded files available in your Go code.
// Execute Generator.Generate from a Go file with go generate
// during build time to.
func (g Generator) Generate() error {
	zipString, err := g.createZipString()
	if err != nil {
		return err
	}
	return g.generateFile(zipString)
}

// generateFile generates Go code with the zip data inside.
func (g Generator) generateFile(zipString string) error {
	template := g.template()
	buf := new(bytes.Buffer)

	g.PackageName = g.packageName()
	g.Name = g.name()
	g.Value = zipString

	err := template.Execute(buf, g)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(g.outputFilePath(), buf.Bytes(), 0644)
}

// Return the specied name or the default value resources.
func (g Generator) name() string {
	if g.Name != "" {
		return g.Name
	}
	return "resources"
}

// The default output file path is Generator.name() + .go extension.
// Overwrite Generator.Name to specify an alternate output file.
func (g Generator) outputFilePath() string {
	name := "./" + g.name() + ".go"
	if g.OutputFilePath != "" {
		if filePath, err := filepath.Abs(g.OutputFilePath); err == nil {
			name = filePath
		}
	}
	absPath, err := filepath.Abs(name)
	if err != nil {
		return name
	}
	return absPath
}

// Defaults to the parent directory name of the generated file.
// This should be Ok in most cases. Overwrite Generator.PackageName
// if you have a special use case.
func (g Generator) packageName() string {
	if g.PackageName != "" {
		return g.PackageName
	}
	dir := filepath.Dir(g.outputFilePath())
	return filepath.Base(dir)
}

// emzip comes with a default template for generating the Go file.
// Use Generator.Template to customize the generated code.
// Templates are based on the text/template language, all public
// fields of the Generator struct will be passed in as variables.
func (g Generator) template() *template.Template {
	if g.Template != "" {
		return template.Must(template.New("").Parse(g.Template))
	}
	return template.Must(template.New("").Parse(defaultTemplate))
}

// Read all files specified by the includes array. Files will be
// included directly, directories will be added recursively.
// Returns a Go string containing the resulting byte array encoded
// as hex literals.
func (g Generator) createZipString() (string, error) {
	if g.Includes == nil || len(g.Includes) == 0 {
		return "", fmt.Errorf("No includes specified")
	}

	zipBytes, err := g.createZip()
	if err != nil {
		return "", err
	}
	hexLiterals := make([]string, len(zipBytes))
	for i, b := range zipBytes {
		hexLiterals[i] = fmt.Sprintf("\\x%02x", b)
	}

	return strings.Join(hexLiterals, ""), nil
}

// Create zip from all files specified by includes.
func (g Generator) createZip() ([]byte, error) {
	buffer := new(bytes.Buffer)
	writer := zip.NewWriter(buffer)

	for _, include := range g.Includes {
		filesToInclude, err := g.listFiles(include)
		if err != nil {
			return nil, err
		}

		for _, f := range filesToInclude {
			isDir := strings.HasSuffix(f, "/")
			name := relPath(include, f)
			if isDir {
				name = name + "/"
			}
			log.Printf("Adding %v", name)
			zipEntry, err := writer.Create(name)
			if err != nil {
				return nil, err
			}
			fileBytes := []byte{}
			if !isDir {
				fileBytes, err = ioutil.ReadFile(f)
				if err != nil {
					return nil, err
				}
			}
			_, err = zipEntry.Write(fileBytes)
			if err != nil {
				return nil, err
			}
		}
	}
	writer.Close()
	result := buffer.Bytes()
	return result, nil
}

func relPath(basePath, targetPath string) string {
	absBasePath, err := filepath.Abs(basePath)
	if err != nil {
		panic(err)
	}
	absTargetPath, err := filepath.Abs(targetPath)
	if err != nil {
		panic(err)
	}

	rel, err := filepath.Rel(filepath.Dir(absBasePath), absTargetPath)
	if strings.HasPrefix(rel, "..") {
		rel = rel[3:]
	}
	if err != nil {
		panic(err)
	}
	return filepath.ToSlash(rel)
}

func (g Generator) listFiles(name string) ([]string, error) {
	fileInfo, err := os.Stat(name)
	if err != nil {
		return nil, err
	}
	if fileInfo.Mode().IsRegular() {
		return []string{name}, nil
	}
	result := []string{}
	filepath.Walk(
		name,
		func(path string, fi os.FileInfo, err error) error {
			p := path
			if fi.Mode().IsDir() {
				p = p + "/"
			}
			result = append(result, p)
			return nil
		},
	)
	return result, nil
}

// Default generator template, may be overwritten with Generator.Template.
const defaultTemplate = `package {{ .PackageName }}

import "gitlab.com/eich/emzip"

/*
 * This is a generated file.
 * !! DO NOT EDIT !!
 * Run "go generate" instead...
 */

var {{ .Name }} = emzip.NewFileAccess([]byte("{{ .Value }}"))
`
