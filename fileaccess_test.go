package emzip

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestFileAccess(t *testing.T) {
	test := newFileAccessTest(t)

	test.CheckFile("/some.md", "# Keep it simple")
	test.CheckFile("/web/index.html", "<!doctype html>")
	test.CheckFile("/web/css/style.css", "body { display: flex; }")
	test.CheckFile("/web/favicon.png", "No real image")

	_, err := test.fa.ReadFile("/not/found")
	if err == nil {
		t.Error("Expected error, but nothing happened")
	}
}

func TestDir(t *testing.T) {
	test := newFileAccessTest(t)

	staticFiles := test.fa.Dir("/web")
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(staticFiles))
	mux.Handle(
		"/web/",
		http.StripPrefix("/web", http.FileServer(staticFiles)),
	)
	server := httptest.NewServer(mux)
	defer server.Close()

	// This is the handler for /

	htmlIndexHTML := test.Get(server.URL, "/web/index.html")
	test.AssertEquals("<!doctype html>", htmlIndexHTML)

	styleCSS := test.Get(server.URL, "/web/css/style.css")
	test.AssertEquals("body { display: flex; }", styleCSS)

	// This is the handler for /web/

	root := test.Get(server.URL, "/")
	test.AssertEquals("<!doctype html>", root)

	rootIndex := test.Get(server.URL, "/index.html")
	test.AssertEquals("<!doctype html>", rootIndex)

	favicon := test.Get(server.URL, "/favicon.png")
	test.AssertEquals("No real image", favicon)

	docIndex := test.Get(server.URL, "/doc")
	test.AssertEquals("Documentation Index", docIndex)

	test.NotFound(server.URL, "/none/index.html")
}

func newFileAccessTest(t *testing.T) fileAccessTest {
	zipData, err := Generator{
		Includes: []string{"test/web", "test/some.md"},
	}.createZip()

	if err != nil {
		t.Error(err)
	}

	fa := NewFileAccess(zipData)

	return fileAccessTest{t, fa}
}

type fileAccessTest struct {
	t  *testing.T
	fa *FileAccess
}

func (test fileAccessTest) Get(url string, path string) string {
	res, err := http.Get(url + path)
	test.FailOnError(err)

	result, err := ioutil.ReadAll(res.Body)
	test.FailOnError(err)

	return string(result)
}

func (test fileAccessTest) NotFound(url string, path string) {
	res, err := http.Get(url + path)
	test.FailOnError(err)

	if res.StatusCode != http.StatusNotFound {
		test.t.Errorf("Expected 404, got %v", res.StatusCode)
	}
}

func (test fileAccessTest) CheckFile(name string, text string) {
	data, err := test.fa.ReadFile(name)

	test.FailOnError(err)

	actualText := string(data)
	if actualText != text {
		test.t.Errorf("Expected %v, Got %v", text, actualText)
	}
}

func (test fileAccessTest) FailOnError(err error) {
	if err != nil {
		test.t.Errorf("Test failed on error: %v", err)
	}
}

func (test fileAccessTest) AssertEquals(expected string, actual string) {
	if expected != actual {
		test.t.Errorf("Expected %v, Got %v", expected, actual)
	}
}
