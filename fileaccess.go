package emzip

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

// FileAccess allows accessing files within the embedded resources.
type FileAccess struct {
	data  []byte
	index map[string]*zipFile
}

// NewFileAccess creates a file accessor based on the
// given zip data.
func NewFileAccess(zipData []byte) *FileAccess {
	reader, err := zip.NewReader(bytes.NewReader(zipData), int64(len(zipData)))
	if err != nil {
		log.Panicf("Internal error, could not read embedded resources, %v", err)
	}
	index := make(map[string]*zipFile)
	for _, file := range reader.File {
		absPath := "/" + (*file).Name
		index[absPath] = &zipFile{f: file}
	}
	return &FileAccess{data: zipData, index: index}
}

// Dir returns an http.FileSystem implementation that
// allows access to files and directories inside of the
// contained zip - just like http.Dir does with the
// real file system.
// All files within root will be available.
func (fa FileAccess) Dir(root string) http.FileSystem {
	rootDir := root
	if !strings.HasPrefix(rootDir, "/") {
		rootDir = "/" + rootDir
	}
	dirIndex := make(map[string]*zipFile)
	for absPath, file := range fa.index {
		if strings.HasPrefix(absPath, rootDir) {
			dirIndex[absPath[len(rootDir):]] = file
		}
	}
	return zipDir{dirIndex}
}

// ReadFile returns the whole contents of the given file as a byte array.
func (fa FileAccess) ReadFile(name string) ([]byte, error) {
	file, ok := fa.index[name]
	if !ok {
		return []byte{}, fmt.Errorf("not found: %v", name)
	}
	reader, err := file.Open()
	if err != nil {
		return nil, err
	}
	var buf bytes.Buffer
	_, err = io.Copy(&buf, reader)
	return buf.Bytes(), err
}

// Extract writes a file from the embedded zip data to disk.
func (fa FileAccess) Extract(name string, targetName string) error {
	file, ok := fa.index[name]
	if !ok {
		return fmt.Errorf("Could not open %v", name)
	}
	f, err := file.Open()
	if err != nil {
		return err
	}
	outFile, err := os.Create(targetName)
	if err != nil {
		return err
	}
	_, err = io.Copy(outFile, f)
	return err
}

type zipDir struct {
	index map[string]*zipFile
}

// Open implements the http.FileSystem interface
func (z zipDir) Open(name string) (http.File, error) {
	file, ok := z.index[name]
	if !ok {
		if indexHTML, ok := z.index[name+"/index.html"]; ok {
			file = indexHTML
		} else {
			return nil, os.ErrNotExist
		}
	}
	reader, err := file.Open()
	if err != nil {
		return nil, err
	}
	file.reader = reader
	return file, nil
}

type zipFile struct {
	f       *zip.File
	dirName string
	index   map[string]*zipFile
	reader  io.ReadCloser
}

func (f *zipFile) Open() (io.ReadCloser, error) {
	return f.f.Open()
}

func (f *zipFile) Read(p []byte) (int, error) {
	if f.reader == nil {
		return 0, fmt.Errorf("file is not open")
	}
	return f.reader.Read(p)
}

func (f *zipFile) Close() error {
	return f.reader.Close()
}

func (f *zipFile) Seek(offset int64, whence int) (int64, error) {
	return 0, nil
}

func (f *zipFile) Readdir(count int) ([]os.FileInfo, error) {
	return nil, nil
}

func (f *zipFile) Stat() (os.FileInfo, error) {
	return f.f.FileInfo(), nil
}
