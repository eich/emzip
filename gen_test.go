package emzip

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestParamCombination(t *testing.T) {
	outputFilePath := "test/otherfile.go"
	defer os.Remove(outputFilePath)

	text := generate(t, Generator{
		PackageName:    "emzip_test",
		Name:           "embeddedFiles",
		OutputFilePath: outputFilePath,
		Includes:       []string{"test/web", "test/some.md"},
	}, outputFilePath)

	assertPackage(t, text, "emzip_test")
	assertVarName(t, text, "embeddedFiles")
}

func TestGenerateWithDefaults(t *testing.T) {
	outputFilePath := "resources.go"
	defer os.Remove(outputFilePath)

	text := generate(t, Generator{
		Includes: []string{"test/web", "test/some.md"},
	}, outputFilePath)

	assertPackage(t, text, "emzip")
	assertVarName(t, text, "resources")
}

func TestWithPackageName(t *testing.T) {
	outputFilePath := "resources.go"
	defer os.Remove(outputFilePath)

	text := generate(t, Generator{
		PackageName: "emzip_test",
		Includes:    []string{"test/web"},
	}, outputFilePath)

	assertPackage(t, text, "emzip_test")
	assertVarName(t, text, "resources")
}

func TestWithVarName(t *testing.T) {
	outputFilePath := "embeddedData.go"
	defer os.Remove(outputFilePath)

	text := generate(t, Generator{
		Name:     "embeddedData",
		Includes: []string{"test/web"},
	}, "embeddedData.go")

	assertPackage(t, text, "emzip")
	assertVarName(t, text, "embeddedData")
}

func TestWithOtherOutputFile(t *testing.T) {
	outputFilePath := "test/otherfile.go"
	defer os.Remove(outputFilePath)

	text := generate(t, Generator{
		Includes:       []string{"test/some.md"},
		OutputFilePath: outputFilePath,
	}, outputFilePath)

	assertPackage(t, text, "test")
	assertVarName(t, text, "resources")
}

func TestOtherTemplate(t *testing.T) {
	outputFilePath := "resources.go"
	defer os.Remove(outputFilePath)

	text := generate(t, Generator{
		Includes: []string{"test/some.md"},
		Template: "Expected file content",
	}, "resources.go")

	if text != "Expected file content" {
		t.Errorf("Expected other file content, got: %v", text)
	}
}

func generate(t *testing.T, g Generator, outputFile string) string {
	err := g.Generate()

	if err != nil {
		t.Error(err)
	}

	data, err := ioutil.ReadFile(outputFile)
	if err != nil {
		t.Error(err)
	}

	return string(data)
}

func assertPackage(t *testing.T, text string, name string) {
	if strings.Index(text, "package "+name) < 0 {
		t.Errorf("Expected package %v, got %v", name, text)
	}
}
func assertVarName(t *testing.T, text string, name string) {
	if strings.Index(text, fmt.Sprintf("var %v = ", name)) < 0 {
		t.Errorf("Expected var %v, got %v", name, text)
	}

}
