// Package emzip helps to embed static files in a compiled
// Go executable.
//
// Use the Generator to create a Go source file with all
// embedded files.
//
// Example (gen.go)
//
//   // +build ignore
//
//   package main
//
//   import "gitlab.com/eich/emzip"
//
//   func main() {
//       emzip.Generator{
//           PackageName: "main",
//           Includes: []string{"static", "otherfile.txt"},
//       }.Generate()
//   }
//
// This will include otherfile.txt and the static directory (recursively)
// as embedded resources. Just add a go:generate declaration in you code
// and run go generate
//
//   //go:generate go run gen.go
//
// By default, a file named resources.go will be generated:
//
//   package main
//
//   import "gitlab.com/eich/emzip"
//
//   /*
//    * This is a generated file.
//    * !! DO NOT EDIT !!
//    * Run "go generate" instead...
//    */
//
//   var resources = emzip.NewFileAccess([]byte(/* ... */))
//
// Now, you can use resources to access the embedded files in your
// code.
//
// Reading Files
//   // data, err := resources.ReadFile("/otherfile.txt")
//   if err != nil {
//       // handle err, probably file not found.
//   }
//   // data []byte contains all bytes of the file
//
// Extracting Files
//
//   err := resources.Extract("/otherfile.txt", "path/to/otherfile.txt")
//   if err != nil {
//       // handle err
//   }
//   // otherfile.txt is now extracted to disk
//
// http.FileSystem
//
// FileAccess.Dir works exactly like http.Dir. To serve a directory, simply
// pass it to http.FileServer:
//
//   http.Handle("/", resources.Dir("static"))
//
// During development, it is helpful to serve files directly from the
// file system. In this case, you can make a simple switch using a
// command line parameter or build tag:
//
//   func staticFiles() http.FileSystem {
//       if useFileSystem {
//           return http.Dir("static")
//       }
//       return resources.Dir("static")
//   }
//   // ...
//   http.Handle("/", http.FileServer(staticFiles()))
//
// Use http.StripPrefix for path corrections.
package emzip
