# emzip

Embed static files in a Go-compiled executable.

## Motivation

Statically linked executables are an awesome feature of the Go
programming language.

This library helps you to bundle static resources (e.g. HTML pages)
inside your single executable.

## Installation

```text
> go get -u gitlab.com/eich/emzip
```

## Generator

The _emzip_ workflow is based on `go generate`.

First, create a _Go_ file that will configure and run the `Generator`: 

```go
// +build ignore

package main

import "gitlab.com/eich/emzip"

func main() {
    emzip.Generator{
        PackageName: "main",
        Includes: []string{"static", "otherfile.txt"},
    }.Generate()
}
```

Add a `go:generate` comment to your source code:

```go
//go:generate go run gen.go
```

After running `go generate`, the file `resources.go` will be generated.
It contains a single variable holding zipped data of all included resources.
In this example, the `static` directory will be added recursively and
`otherfile.txt` is added directly.

```go
package main

import "gitlab.com/eich/emzip"

/*
 * This is a generated file.
 * !! DO NOT EDIT !!
 * Run "go generate" instead...
 */

var resources = emzip.NewFileAccess([]byte(/* ... */))
```

The `emzip.Generator` type offers these fields to customize the
code generation:

- `PackageName`: Defaults to the name of the parent directory. Use this
  field to overwrite package name in special cases, e.g. `main`.
- `Name`: The name of the variable defaults to `"resources"`.
- `OutputFilePath`: relative or absolute path to the file to generate.
  Defaults to the `Name` param followed by `.go`.
- `Includes`: holds all names of files and directories to add. Directories
  will always be included recursively.

As you can see, `resources` has the type `emzip.FileAccess`.
This will allows you to conveniently access embedded files from your Go code.

## File Access

All snippets here assume that the embedded files are accessible via
`resources`. If you have renamed that variable in the generator settings,
please adapt.

### Reading Files

```go
data, err := resources.ReadFile("/otherfile.txt")
if err != nil {
    // handle err, probably file not found.
}
// data []byte contains all bytes of the file 
```

### Extracting Files

If you need to extract a file to the filesystem:

```go
err := resources.Extract("/otherfile.txt", "path/to/destination/otherfile.txt")
if err != nil {
    // handle err
}
// otherfile.txt is now extracted to disk
```

### `http.FileSystem`

`FileAccess.Dir` works exactly like `http.Dir`. To serve a directory, simply
pass it to `http.FileServer`:

```go
http.Handle("/", resources.Dir("static"))
```

During development, it is helpful to serve files directly from the
file system. In this case, you can make a simple switch using a
command line parameter or build tag:

```go
func staticFiles() http.FileSystem {
    if useFileSystem {
        return http.Dir("static")
    }
    return resources.Dir("static")
}
// ...
http.Handle("/", http.FileServer(staticFiles()))
```

Use `http.StripPrefix` for path corrections.